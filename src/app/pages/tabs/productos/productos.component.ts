import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Account } from 'src/interfaces/account.interface';
import { CreditCard } from 'src/interfaces/credit-card.interface';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss'],
})
export class ProductosComponent implements OnInit {

  accounts : Account[] = [];
  creditCards : CreditCard[] =[];

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.getAccounts();
    this.getCreditCards();
  }

  ionViewDidEnter(){
    this.getAccounts();
    this.getCreditCards();
  }

  getAccounts(){
    if(this.userService.usuario == null){
      this.userService.getAccounts()
        .subscribe( resp =>{
          this.accounts=resp;
      }, err => {
          console.log(err);
      });
    }
  }


  getCreditCards(){
    if(this.userService.usuario == null){
      this.userService.getCreditCards()
        .subscribe( resp =>{
          this.creditCards=resp;
      }, err => {
          console.log(err);
      });
    }
  }

}
