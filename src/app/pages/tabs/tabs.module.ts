
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ConfiguracionesComponent } from './configuraciones/configuraciones.component';
import { OfertasComponent } from './ofertas/ofertas.component';
import { ProductosComponent } from './productos/productos.component';
import { TransaccionesComponent } from './transacciones/transacciones.component';
import { PagesComponent } from './tabs.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorService } from 'src/app/interceptor/interceptor.service';
import { ContactoComponent } from '../contacto/contacto.component';
import { SucursalesComponent } from '../sucursales/sucursales.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  providers: [
    {
      provide : HTTP_INTERCEPTORS,
      useClass : InterceptorService,
      multi: true
    }
  ],
  declarations: [
    ConfiguracionesComponent,
    OfertasComponent,
    ProductosComponent,
    ContactoComponent,
    SucursalesComponent,
    PagesComponent,
    TransaccionesComponent]
})
export class TabsModule {}
