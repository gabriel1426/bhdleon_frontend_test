import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './tabs.component';
//import { AuthGuard } from '../guards/auth.guard';
import { ProductosComponent } from './productos/productos.component';
import { ConfiguracionesComponent } from './configuraciones/configuraciones.component';
import { OfertasComponent } from './ofertas/ofertas.component';
import { TransaccionesComponent } from './transacciones/transacciones.component';
import { AuthGuard } from 'src/app/guards/auth.guard';


const routes: Routes = [
    {  
        path: 'dashboard', 
        canActivate: [ AuthGuard ],
        component:PagesComponent,
        children: [
            {
                path: '',
                component: ProductosComponent
            },
            {
                path: 'productos',
                component: ProductosComponent
            },
            {
            path: 'configuraciones',
            component: ConfiguracionesComponent
            }
            ,
            {
            path: 'ofertas',
            component: OfertasComponent
            }
            ,
            {
            path: 'transacciones',
            component: TransaccionesComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TabsRoutingModule {}
