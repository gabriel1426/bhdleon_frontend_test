import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-pages',
  templateUrl: './tabs.component.html',
})
export class PagesComponent implements OnInit {

  constructor(private userService: UserService, private events: Events) {

    this.getUserData();

   }

  ngOnInit() {}

  getUserData() { 
    if(this.userService.usuario == null){
      this.userService.getUserData()
        .subscribe( resp =>{
          console.log(resp)
          this.events.publish('user:created', resp, Date.now());
          this.userService.usuario=resp;
      }, err => {
          console.log(err);
      });
    }
  }
  
}
