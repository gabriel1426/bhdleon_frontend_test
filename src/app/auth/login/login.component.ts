import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = this.fb.group({
    userId: ['', Validators.required],
    password: ['', Validators.required]
  });

  constructor(private router:Router, private fb: FormBuilder,  private userService: UserService) {

   }

  ngOnInit() {}

  onSubmit() { 
    if(this.loginForm.valid){
      this.userService.login(this.loginForm.value)
        .subscribe( resp =>{
          this.router.navigateByUrl('/');
      }, err => {
        console.log(err);
      });
    }
  }

}
