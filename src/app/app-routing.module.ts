import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthRoutingModule } from './auth/auth.routing';
import { ContactoComponent } from './pages/contacto/contacto.component';
import { SucursalesComponent } from './pages/sucursales/sucursales.component';
import { TabsRoutingModule } from './pages/tabs/tabs.routing';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'sucursales', component: SucursalesComponent },
  { path: 'contacto', component: ContactoComponent },
];

@NgModule({
  imports: [
    
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
    TabsRoutingModule, 
    AuthRoutingModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
