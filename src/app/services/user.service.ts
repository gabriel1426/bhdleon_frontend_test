import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators'
import { LoginForm } from 'src/interfaces/login-form.interface';
import { User } from 'src/interfaces/user.interface';
import { Observable } from 'rxjs';
import { Account } from 'src/interfaces/account.interface';
import { CreditCard } from 'src/interfaces/credit-card.interface';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  usuario:User;

  constructor(private http: HttpClient) { }

  login (loginForm:LoginForm){
    return this.http.post(`${base_url}/sign_in`,loginForm)
      .pipe(
        tap ( (resp: any) =>{
          localStorage.setItem('token', resp.access_token);
        })
      );
  }

  // Dado que no hay un metodo en api para validar el token 
  // solo voy a validar si existe si el local storage
  validateUser(){
    const token = localStorage.getItem('token') || '';
    return token !== '';    
  }

  getUserData ():Observable<User>{
    return this.http.get<User>(`${base_url}/user_data`);
  }

  getAccounts ():Observable<Account[]>{
    return this.http.get<Account[]>(`${base_url}/products/accounts`);
  }

  getCreditCards ():Observable<CreditCard[]>{
    return this.http.get<CreditCard[]>(`${base_url}/products/credit_cards`);
  }
}
