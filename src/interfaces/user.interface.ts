
export interface User {

    lastName: string,
    name: string,
    photo: string

}