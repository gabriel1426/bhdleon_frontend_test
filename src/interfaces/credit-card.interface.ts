export interface CreditCard {

    alias: string;
    number: number;
    availableAmountRD: number;
    availableAmountUS: number;
    isInternational: boolean;
    productType: string;

}