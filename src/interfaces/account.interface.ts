export interface Account {

    alias: string;
    number: number;
    availableAmount: number;
    productType: string;

}