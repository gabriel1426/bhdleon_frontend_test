import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.bhdleon_frontend_test.app',
  appName: 'bhdleon-frontend-test',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
